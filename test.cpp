#include <stdio.h>

int sum(int a, int b)
{
    return a+b;
}

int main()
{
    printf("Test\n");

    if(sum(1,2) ==3)
        printf("Success\n");
    else
        printf("Fail\n");

    if(sum(1,2) == 5)
        printf("Success\n");
    else
        printf("Fail\n");        

    return 0;

}